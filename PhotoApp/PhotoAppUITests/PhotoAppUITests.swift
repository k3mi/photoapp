//
//  PhotoAppUITests.swift
//  PhotoAppUITests
//
//  Created by Oluwakemi Borisade on 26/11/2015.
//  Copyright © 2015 Oluwakemi Borisade. All rights reserved.
//

import XCTest

class PhotoAppUITests: XCTestCase {
        
    override func setUp() {
        super.setUp()
        
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false
        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        XCUIApplication().launch()

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testAddpicture() {
        
        
        
        let app = XCUIApplication()
        app.toolbars.buttons["Organize"].tap()
        app.tables.buttons["Camera Roll"].tap()
        app.collectionViews.cells["Photo, Landscape, 12:22 PM"].tap()
        app.childrenMatchingType(.Window).elementBoundByIndex(0).childrenMatchingType(.Other).element.childrenMatchingType(.Other).element.tap()
        
    }
    
   
    func testImage() {
        let app = XCUIApplication()
        app.otherElements.containingType(.NavigationBar, identifier:"Image").childrenMatchingType(.Other).element.childrenMatchingType(.Other).element.childrenMatchingType(.Other).element.childrenMatchingType(.Image).element.tap()
        app.childrenMatchingType(.Window).elementBoundByIndex(0).childrenMatchingType(.Other).element.childrenMatchingType(.Other).element.childrenMatchingType(.Other).element.childrenMatchingType(.Other).element.tap()
        app.toolbars.buttons["Facebook"].tap()
        
    }
    func testCamera() {
        
        let app = XCUIApplication()
        app.navigationBars["Photo Album"].buttons["Camera"].tap()
        app.alerts["Error!"].collectionViews.buttons["Okay"].tap()
    
        
        
        
//        let app = XCUIApplication()

        
        
        
        
        /*let app = XCUIApplication()
        app.otherElements.containingType(.NavigationBar, identifier:"Image").childrenMatchingType(.Other).element.childrenMatchingType(.Other).element.childrenMatchingType(.Other).element.childrenMatchingType(.Image).element.tap()
        app.childrenMatchingType(.Window).elementBoundByIndex(0).childrenMatchingType(.Other).element.childrenMatchingType(.Other).element.childrenMatchingType(.Other).element.childrenMatchingType(.Other).element.tap()
        app.toolbars.buttons["Facebook"].tap()*/
        
        
        
        
       /* let app = XCUIApplication()
        let photoappPhotoviewNavigationBar = app.navigationBars["PhotoApp.PhotoView"]
        photoappPhotoviewNavigationBar.buttons["Organize"].tap()
        app.tables.buttons["Moments"].tap()
        app.collectionViews.cells["Photo, Landscape, August 08, 2012, 10:55 PM"].tap()
        app.toolbars.buttons["Change filter "].tap()
        app.sheets["Choose filter"].collectionViews.buttons["CICircularWrap"].tap()
        photoappPhotoviewNavigationBar.buttons["Save to Album"].tap()*/
        
        
    }
}

//
//  PhotoView.swift
//  PhotoApp
//
//  Created by Oluwakemi Borisade on 03/12/2015.
//  Copyright © 2015 Oluwakemi Borisade. All rights reserved.
//

import UIKit
import AssetsLibrary
import Photos
import Social
 
class PhotoView: UIViewController,  UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    @IBOutlet weak var picture: UIImageView!
    
    @IBOutlet weak var amSlider: UISlider!
  
    
    var filter:CIFilter!
    var context: CIContext!
    var beginImage: CIImage!
    var extent: CGRect!
    var assetCollections: PHAssetCollection!
    var photoAsset: PHFetchResult!
    var theIndex: Int = 0

  
    
    


    override func viewDidLoad() {
        super.viewDidLoad()
    
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    override func viewWillAppear(animated: Bool) {
        self.navigationController?.hidesBarsOnTap = true
        self.displayPic()
    }
    
    
    //function to display picture selected by user
    func displayPic (){
        let screen: CGSize = UIScreen.mainScreen().bounds.size
        let target = CGSizeMake(screen.width, screen.height)
        let imageManager = PHImageManager.defaultManager()
        
        if let theasset = self.photoAsset[self.theIndex] as? PHAsset{
            imageManager.requestImageForAsset(theasset, targetSize: target, contentMode: .AspectFit, options: nil, resultHandler:{(result, info)-> Void in self.picture.image = result
        
            })
    
      }
    }
  

    
    //deletes photo from app specific folder
    @IBAction func deleteButton(sender: AnyObject) {
        let alert = UIAlertController(title: "Delete Image", message: "Are you sure?", preferredStyle: .Alert)
        alert.addAction(UIAlertAction(title: "Yes", style: .Default, handler: {(alertAction) in
            PHPhotoLibrary.sharedPhotoLibrary().performChanges({
                //Delete Photo
                if let therequest = PHAssetCollectionChangeRequest(forAssetCollection: self.assetCollections){
                    therequest.removeAssets([self.photoAsset[self.theIndex]])
                }
                },
                completionHandler: {(success, error)in
                    NSLog("\nImage -> %@", (success ? "Successfully deleted":"Error!"))
                    alert.dismissViewControllerAnimated(true, completion: nil)
                    if(success){
                        // Move to the main thread to execute
                        dispatch_async(dispatch_get_main_queue(), {
                            self.photoAsset = PHAsset.fetchAssetsInAssetCollection(self.assetCollections, options: nil)
                            if(self.photoAsset.count == 0){ //if user deletes the last image.
                                print("No Photos Left!!")
                                self.navigationController?.popToRootViewControllerAnimated(true) 
                            }else{
                                if(self.theIndex >= self.photoAsset.count){ //when user deletes photo show next photo in the index
                                    self.theIndex = self.photoAsset.count - 1
                                }
                                self.displayPic()
                            }
                        })
                    }else{
                        print("Error: \(error)")
                    }
            })
        }))
        
        alert.addAction(UIAlertAction(title: "No", style: .Cancel, handler: {(alertAction) in
            alert.dismissViewControllerAnimated(true, completion: nil)
        }))
        self.presentViewController(alert, animated: true, completion: nil)
    }
    
    //save picture to app specific folder when save button is clicked
    
    @IBAction func savePicture(sender: AnyObject) {
       PHPhotoLibrary.sharedPhotoLibrary().performChanges({
            let assetReq = PHAssetChangeRequest.creationRequestForAssetFromImage(self.picture.image!)
            let assetPlaceholder = assetReq.placeholderForCreatedAsset
            let albumChangeRequest = PHAssetCollectionChangeRequest(forAssetCollection:self.assetCollections, assets: self.photoAsset)
            albumChangeRequest!.addAssets([assetPlaceholder!])}, completionHandler: { success, error in
                print ("image saved")
                print (error)
                let alert = UIAlertController(title: "Image saved" ,
                    message: "Image has been saved to album",
                    preferredStyle: UIAlertControllerStyle.Alert)
                let cancelAction = UIAlertAction(title: "OK",
                    style: .Cancel, handler: nil)
                alert.addAction(cancelAction)
                self.presentViewController(alert, animated: true, completion: nil)
               
        })
        
    }
    

  //return user to previous view
    @IBAction func cancelButton(sender: AnyObject) {
        self.navigationController?.popToRootViewControllerAnimated(true)
    }
    
    //allow user to select from one of these filters
    @IBAction func changeFilter(sender: AnyObject) {
        let alertCtrl = UIAlertController(title: "Choose filter", message: nil, preferredStyle: .ActionSheet)
        alertCtrl.addAction(UIAlertAction(title: "CIBumpDistortion", style: .Default, handler: applyFilter))
        alertCtrl.addAction(UIAlertAction(title: "CISepiaTone", style: .Default, handler: applyFilter))
        alertCtrl.addAction(UIAlertAction(title: "CIVignette", style: .Default, handler: applyFilter))
        alertCtrl.addAction(UIAlertAction(title: "CIPhotoEffectChrome", style: .Default, handler: applyFilter))
        alertCtrl.addAction(UIAlertAction(title: "CICircularWrap", style: .Default, handler: applyFilter))
        alertCtrl.addAction(UIAlertAction(title: "CIHoleDistortion", style: .Default, handler: applyFilter))
        alertCtrl.addAction(UIAlertAction(title: "Cancel", style: .Cancel, handler: nil))
        presentViewController(alertCtrl, animated: true, completion: nil)
    }
    
    //apply filter that has been selected by user
    func applyFilter (action: UIAlertAction!) {
        self.filter = CIFilter(name: action.title!)
        beginImage = CIImage (image:self.picture.image!)
        
        self.filter.setValue(beginImage, forKey: kCIInputImageKey)
        context = CIContext(options: nil)
        let cgimg = context.createCGImage(self.filter.outputImage!, fromRect: self.filter.outputImage!.extent)
        self.picture.image = UIImage(CGImage:cgimg)

    }
    
    
    @IBAction func facebookShare(sender: AnyObject) {
        let facebookSharee : SLComposeViewController = SLComposeViewController(forServiceType: SLServiceTypeFacebook)
        self.presentViewController(facebookSharee, animated: true, completion: nil)
        facebookSharee.setInitialText("I posted using PhotoApp")
        facebookSharee.addImage(self.picture.image)
        
    
    }
    
    
    @IBAction func twitterShare(sender: AnyObject) {
        let twitterSharee : SLComposeViewController = SLComposeViewController(forServiceType: SLServiceTypeTwitter)
        self.presentViewController(twitterSharee, animated: true, completion: nil)
        twitterSharee.setInitialText("I posted using PhotoApp")
        twitterSharee.addImage(self.picture.image)
        

    }
    
}

//
//  PhotoCollectionView.swift
//  PhotoApp
//
//  Created by Oluwakemi Borisade on 10/12/2015.
//  Copyright © 2015 Oluwakemi Borisade. All rights reserved.
//

import UIKit
import Photos

let reuseIdentifier = "PhotoCell" //name of cell in collection view
let nameAlbum = "Photo App"        //app folder name
class PhotoCollectionView: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    
    var assetCollections: PHAssetCollection = PHAssetCollection()
    var photoAsset: PHFetchResult!
    var foundAlbum: Bool = false
    var assetThumbnail:CGSize!
    var imagePicker: UIImagePickerController!
    
    @IBOutlet var collectionView: UICollectionView!
    
    @IBOutlet var noPhotos: UILabel! //no photos in folder
    
  //function to create app specific folder
    override func viewDidLoad() {
        super.viewDidLoad()
    
        let fetchOp = PHFetchOptions()
        fetchOp.predicate = NSPredicate(format: "title = %@", nameAlbum)
        let collection:PHFetchResult = PHAssetCollection.fetchAssetCollectionsWithType(.Album, subtype: .Any, options: fetchOp)
        
        if let first_Object:AnyObject = collection.firstObject{
            self.foundAlbum = true
            self.assetCollections = first_Object as! PHAssetCollection
            }else{
            //placeholder is where the photos are kept
            var albumHolder: PHObjectPlaceholder!
                NSLog("\nFolder \"%@\" does not exist\nCreating it now...", nameAlbum)
                PHPhotoLibrary.sharedPhotoLibrary().performChanges({
                    let request = PHAssetCollectionChangeRequest.creationRequestForAssetCollectionWithTitle(nameAlbum)
                    albumHolder = request.placeholderForCreatedAssetCollection
                },
                completionHandler: {(success:Bool, error:NSError?)in
                    if(success){
                        print("folder has been created")
                        self.foundAlbum = true
                        let collection = PHAssetCollection.fetchAssetCollectionsWithLocalIdentifiers([albumHolder.localIdentifier], options: nil)
                        self.assetCollections = collection.firstObject as! PHAssetCollection
                    } else {
                            print("Error creating")
                            self.foundAlbum = false
                    }
                
            })
            
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    //customise collectionview layout
    override func viewWillAppear(animated: Bool) {
        if let layout = self.collectionView.collectionViewLayout as? UICollectionViewFlowLayout{
            let cellSize = layout.itemSize
            self.assetThumbnail = CGSizeMake(cellSize.width, cellSize.height)
        }
        self.navigationController?.hidesBarsOnTap = false
        self.photoAsset = PHAsset.fetchAssetsInAssetCollection(self.assetCollections, options: nil)
        
        if let photoCount = self.photoAsset?.count{
            if (photoCount == 0){
                self.noPhotos.hidden = false
            }else{
                self.noPhotos.hidden = true
            }
      }
        self.collectionView.reloadData()
    }
    
    //get pictures from camera roll gallery
    
   @IBAction func getPictures(sender: UIBarButtonItem) {
        let picker: UIImagePickerController =  UIImagePickerController()
        picker.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
        picker.mediaTypes = UIImagePickerController.availableMediaTypesForSourceType(.PhotoLibrary)!
        picker.delegate = self
        picker.allowsEditing = true
        self.presentViewController(picker, animated: true, completion: nil)
        
    }
    
    //take picture using camera
    @IBAction func takePictures(sender: UIBarButtonItem) {
        if(UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera)){
            //load the camera
            let picker : UIImagePickerController = UIImagePickerController()
            picker.sourceType = UIImagePickerControllerSourceType.Camera
            picker.delegate = self
            picker.allowsEditing = true
            self.presentViewController(picker, animated: true, completion: nil)
        }else{
            //no camera available
            let alert = UIAlertController(title: "Error!", message: "No camera available", preferredStyle: .Alert)
            alert.addAction(UIAlertAction(title: "Okay", style: .Default, handler: {(alertAction)in
                alert.dismissViewControllerAnimated(true, completion: nil)
            }))
            self.presentViewController(alert, animated: true, completion: nil)
        }
        
    }
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
   
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
        var count: Int = 0
        if(self.photoAsset != nil){
            count = self.photoAsset.count
        }
        return count;
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell{
        let cell: Thumbnail = collectionView.dequeueReusableCellWithReuseIdentifier(reuseIdentifier, forIndexPath: indexPath)as! Thumbnail
        
        if  let thumb = self.assetThumbnail{
            let asset: PHAsset = self.photoAsset[indexPath.item]as! PHAsset
            PHImageManager.defaultManager().requestImageForAsset(asset, targetSize: thumb, contentMode: .AspectFill, options: nil, resultHandler: {(result, info)in
            if let image = result {
                cell.setThumbnailImage(image)
            }
        })
        }
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat{
        return 4
    }
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAtIndex section: Int)-> CGFloat{
        return 1
    }
    //send thumbnail chosen from collectionView to main View
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if (segue.identifier! as String == "showLargePhoto") {
            if let controller:PhotoView = segue.destinationViewController as? PhotoView{
                if let cell = sender as? UICollectionViewCell{
                    if let indexPath: NSIndexPath = self.collectionView.indexPathForCell(cell)!{
            controller.theIndex = indexPath.item
            controller.photoAsset = self.photoAsset
            controller.assetCollections = self.assetCollections
        
        }
    }
            }
        }
    }
    //implement image picker for when the user selects photos from the library and adds it to the app folder
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]){
        if let image: UIImage = info["UIImagePickerControllerOriginalImage"] as? UIImage{
    
            let priority = DISPATCH_QUEUE_PRIORITY_DEFAULT
            dispatch_async(dispatch_get_global_queue(priority, 0), {
            PHPhotoLibrary.sharedPhotoLibrary().performChanges({
            let createAssetRequest = PHAssetChangeRequest.creationRequestForAssetFromImage(image) //add image to asset
            let assetPlaceholder = createAssetRequest.placeholderForCreatedAsset
                if let albumChangeRequest = PHAssetCollectionChangeRequest(forAssetCollection: self.assetCollections, assets: self.photoAsset) {
                    albumChangeRequest.addAssets([assetPlaceholder!])
                }
                }, completionHandler: {(success, error)in
                   dispatch_async(dispatch_get_main_queue(), {
    NSLog("Adding Image to Library -> %@", (success ? "Sucess":"Error!"))
    picker.dismissViewControllerAnimated(true, completion: nil)
    })
    })
    
    })
    }
    }
    func imagePickerControllerDidCancel(picker: UIImagePickerController){
        picker.dismissViewControllerAnimated(true, completion: nil)
    }

    override func supportedInterfaceOrientations() -> UIInterfaceOrientationMask{
        return .All
    }
}
//
//  Thumbnail.swift
//  PhotoApp
//
//  Created by Oluwakemi Borisade on 10/12/2015.
//  Copyright © 2015 Oluwakemi Borisade. All rights reserved.
//

import UIKit

class Thumbnail: UICollectionViewCell { //create class for photocell
    
    
    @IBOutlet var theView: UIImageView!
    
    func setThumbnailImage (thumbnailImage: UIImage) {
        self.theView.image = thumbnailImage //insert image into the photocell
    }
    
}
